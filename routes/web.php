<?php

use App\Http\Middleware\CheckMember;
use App\Http\Middleware\CheckAdmin;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home Page
Route::get('/', 'pagesController@home');

// Route Filter
Route::get('filter/{id}','pagesController@filter')->name('filter');

// About Page
Route::get('/about', 'pagesController@about');

// Shop Product Page
Route::get('/shop', 'ShopController@index');

// // Detail Product Page
Route::get('/shop/{id}', 'ShopController@show');
// Melakukan Pencarian 
Route::get('/shop/cari','ShopController@search');
//Filter kategori
Route::get('/shop/kategori/{id}', 'ShopController@kategori');

// Cart Page
Route::group(['prefix' => 'cart', 'middleware' => ['member']], function()
{
	Route::get('/', 'CartController@index');
	Route::post('addtocart', 'CartController@store');
	Route::get('delete/{id}', 'CartController@destroy');
	Route::get('checkout', 'CartController@checkout');
	Route::post('checkout/purchase', 'OrderController@purchase');
});

Route::get('getcart', 'pagesController@getdata');

// Login and Register Page
Route::get('/login-register', 'pagesController@loginregister')->name('login-custom');

// Profile Page
Route::get('/profile', 'pagesController@profile')->middleware(CheckMember::class);

// Order User Page
Route::group(['prefix' => 'orders', 'middleware' => ['member']], function() {
	Route::get('/', 'pagesController@orders');
	Route::get('/history', 'pagesController@historyorders');
	Route::post('/details/uploadpayment', 'OrderController@uploadpayment');
});

Route::group(['prefix' => 'dashboard', 'middleware' => ['admin']], function()
{
	// Dashboard Home
	Route::get('/', 'Admin\AdminController@index');

	// Category
	Route::get('category', 'Admin\CategoryController@index');
	Route::post('category/add', 'Admin\CategoryController@store');
	Route::post('category/edit/{id}', 'Admin\CategoryController@update');
	Route::get('category/delete/{id}', 'Admin\CategoryController@destroy');
	Route::get('category/loadcategory', 'Admin\CategoryController@loadCategory');

	// Products
	Route::get('products', 'Admin\ProductController@index');
	Route::post('products', 'Admin\ProductController@sort');
	Route::post('products/add', 'Admin\ProductController@store');
	Route::get('products/edit/{id}', 'Admin\ProductController@edit');
	Route::post('products/edit/{id}', 'Admin\ProductController@update');
	Route::get('products/delete/{id}', 'Admin\ProductController@destroy');
	Route::get('products/{sort}', 'Admin\ProductController@sort');

	// Users
	Route::get('users', 'Admin\UserController@index');
	Route::get('users/adduser', 'Admin\UserController@adduser');
	Route::post('users/add', 'Admin\UserController@store');

	// Orders
	Route::get('orders', 'Admin\OrderController@index');
	Route::get('orders/filter/{status}', 'Admin\OrderController@filter')->name('filter');
	Route::get('orders/details-order/{id}', 'Admin\OrderController@show');
	Route::post('orders/update/{id}', 'Admin\OrderController@update');


	// Template
	Route::get('changetemplate', 'Admin\TemplateController@index');
	Route::post('changetemplate/{id}', 'Admin\TemplateController@update');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
